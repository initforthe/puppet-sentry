# frozen_string_literal: true

require 'spec_helper'

describe 'sentry' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile.with_all_deps }
      it { is_expected.to contain_class('sentry::setup') }
      it { is_expected.to contain_class('sentry::install') }
      it { is_expected.to contain_class('sentry::config') }

      it { is_expected.to contain_package('python') }
      it { is_expected.to contain_exec('pip_install_sentry') }

      it { is_expected.to contain_file('/etc/systemd/system/sentry-web.service') }
      it { is_expected.to contain_service('sentry-web').with(enable: true, ensure: 'running') }

      it { is_expected.to contain_file('/etc/systemd/system/sentry-cron.service') }
      it { is_expected.to contain_service('sentry-cron').with(enable: true, ensure: 'running') }

      it { is_expected.to contain_file('/etc/systemd/system/sentry-worker.service') }
      it { is_expected.to contain_service('sentry-worker').with(enable: true, ensure: 'running') }

      # rubocop:disable Style/TrailingCommaInArguments
      it do
        is_expected.to contain_file('/etc/sentry/config.yml').with_content(
          %r{^filestore.backend: 'filesystem'$}
        )
      end

      it do
        is_expected.to contain_file('/etc/sentry/config.yml').with_content(
          %r{filestore\.options:\n  location: '\/tmp\/sentry-files'}
        )
      end

      context "with $filestore_backend = 'foo'" do
        let(:params) { { filestore_backend: 'foo' } }

        it { is_expected.to compile.and_raise_error(%r{one of: filesystem, gcs, s3}) }
      end

      context "with $filestore_backend = 'gcs'" do
        let(:params) { { filestore_backend: 'gcs' } }

        it { is_expected.to compile.and_raise_error(%r{filestore_bucket must be set}) }
      end

      context "with $filestore_backend = 's3'" do
        let(:params) { { filestore_backend: 's3', filestore_bucket: 'foo' } }

        it { is_expected.to compile.and_raise_error(%r{filestore_access_key must be set}) }
      end

      context 'with $web_options set' do
        let(:params) do
          { web_options: { workers: 3, protocol: 'uwsgi' } }
        end

        it do
          is_expected.to contain_file('/etc/sentry/sentry.conf.py').with_content(
            %r{SENTRY_WEB_OPTIONS = \{\n  'workers': 3,\n  'protocol': 'uwsgi',\n\}}
          )
        end
      end
      # rubocop:enable Style/TrailingCommaInArguments
    end
  end
end

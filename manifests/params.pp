# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include sentry::params
class sentry::params {
  # $admin_email           = 'admin@localhost'
  # $admin_password        = 'admin'
  # $admin_user            = 'admin'
  # $bootstrap             = true
  # $custom_config         = undef
  # $custom_settings       = undef
  $database_host         = ''
  $database_name         = 'sentry'
  $database_password     = ''
  $database_port         = ''
  $database_user         = 'sentry'
  $debug                 = false
  $enable_cron           = true
  $enable_worker         = true
  # $extensions            = {}
  $filestore_access_key  = undef
  $filestore_backend     = 'filesystem'
  $filestore_bucket      = undef
  $filestore_location    = '/tmp/sentry-files'
  $filestore_secret_key  = undef
  $group                 = 'sentry'
  $mail_backend          = 'smtp'
  $mail_from             = "sentry@${::fqdn}"
  $mail_host             = 'localhost'
  $mail_mailgun_api_key  = undef
  $mail_password         = ''
  $mail_port             = '25'
  $mail_reply_hostname   = undef
  $mail_use_tls          = false
  $mail_username         = ''
  $manage_memcached      = false
  $manage_postgresql     = false
  $manage_redis          = false
  $memcached_location    = '127.0.0.1:11211'
  $organization          = 'Default'
  $path                  = '/usr/local/sentry'
  $redis_host            = '127.0.0.1'
  $redis_port            = '6379'
  $single_organization   = false
  $system_admin_email    = 'root@localhost'
  $system_secret_key     = fqdn_rand_string(50)
  $use_big_ints          = true
  $use_memcached         = false
  $use_ssl               = true
  $user                  = 'sentry'
  $version               = 'latest'
  $web_host              = '0.0.0.0'
  $web_options           = {}
  $web_port              = '9000'
}

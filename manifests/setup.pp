# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include sentry::setup
class sentry::setup (
  $group         = $sentry::group,
  $path          = $sentry::path,
  $use_memcached = $sentry::use_memcached,
  $user          = $sentry::user,
) {
  assert_private()

  group { $group: ensure => present }

  user { $user:
    ensure  => present,
    gid     => $group,
    home    => '/dev/null',
    shell   => '/bin/false',
    require => Group[$group],
  }

  file { '/var/log/sentry':
    ensure  => directory,
    owner   => $user,
    group   => $group,
    mode    => '0755',
    require => User[$user],
  }

  logrotate::rule { 'sentry':
    ensure       => present,
    path         => '/var/log/sentry/*.log',
    compress     => true,
    rotate       => 5,
    rotate_every => 'week',
  }

  $package_dependencies = $facts['os']['name'] ? {
    /(Debian|Ubuntu)/ => [
      'python-setuptools', # will install python2.7
      'libxslt1-dev',
      'gcc',
      'libffi-dev',
      'libjpeg-dev',
      'libxml2-dev',
      'libxslt-dev',
      'libyaml-dev',
      'libpq-dev'
    ],
    default            => fail("OS ${facts['os']['name']} not supported")
  }

  ensure_packages($package_dependencies)

  class { '::python':
    dev        => present,
    virtualenv => present,
  }

  python::virtualenv { $path:
    ensure  => present,
    owner   => $user,
    group   => $group,
    version => 'system',
  }

  Python::Pip {
    ensure     => present,
    virtualenv => $path
  }
}

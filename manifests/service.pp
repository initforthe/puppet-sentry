# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include sentry::service
class sentry::service (
  $enable_cron   = $sentry::enable_cron,
  $enable_worker = $sentry::enable_worker,
  $group         = $sentry::group,
  $path          = $sentry::path,
  $user          = $sentry::user,
) {
  assert_private()

  $_worker_running = $enable_worker ? {
    true  => 'running',
    false => 'stopped',
  }

  $_cron_running = $enable_cron ? {
    true  => 'running',
    false => 'stopped',
  }

  file { '/etc/systemd/system/sentry-web.service':
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('sentry/service/web.service.erb'),
  }
  -> service {'sentry-web':
    ensure    => 'running',
    enable    => true,
    subscribe => Class['::sentry::config'],
  }

  file { '/etc/systemd/system/sentry-worker.service':
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('sentry/service/worker.service.erb'),
  }
  -> service {'sentry-worker':
    ensure    => $_worker_running,
    enable    => $enable_worker,
    subscribe => Class['::sentry::config'],
  }

  file { '/etc/systemd/system/sentry-cron.service':
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('sentry/service/cron.service.erb'),
  }
  -> service {'sentry-cron':
    ensure    => $_cron_running,
    enable    => $enable_cron,
    subscribe => Class['::sentry::config'],
  }
}

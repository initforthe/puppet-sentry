# Sentry
#
# @summary Installs Sentry from PyPI
#
# @example
#   include sentry
#
# @params

class sentry (
  # $admin_email           = $sentry::params::admin_email,
  # $admin_password        = $sentry::params::admin_password,
  # $admin_user            = $sentry::params::admin_user,
  # $bootstrap             = $sentry::params::bootstrap,
  # $custom_config         = $sentry::params::custom_config,
  # $custom_settings       = $sentry::params::custom_settings,
  $database_host         = $sentry::params::database_host,
  $database_name         = $sentry::params::database_name,
  $database_password     = $sentry::params::database_password,
  $database_port         = $sentry::params::database_port,
  $database_user         = $sentry::params::database_user,
  $debug                 = $sentry::params::debug,
  $enable_cron           = $sentry::params::enable_cron,
  $enable_worker         = $sentry::params::enable_worker,
  # $extensions            = $sentry::params::extensions,
  $filestore_access_key  = $sentry::params::filestore_access_key,
  $filestore_backend     = $sentry::params::filestore_backend,
  $filestore_bucket      = $sentry::params::filestore_bucket,
  $filestore_location    = $sentry::params::filestore_location,
  $filestore_secret_key  = $sentry::params::filestore_secret_key,
  $group                 = $sentry::params::group,
  $mail_backend          = $sentry::params::mail_backend,
  $mail_from             = $sentry::params::mail_from,
  $mail_host             = $sentry::params::mail_host,
  $mail_mailgun_api_key  = $sentry::params::mail_mailgun_api_key,
  $mail_password         = $sentry::params::mail_password,
  $mail_port             = $sentry::params::mail_port,
  $mail_reply_hostname   = $sentry::params::mail_reply_hostname,
  $mail_use_tls          = $sentry::params::mail_use_tls,
  $mail_username         = $sentry::params::mail_username,
  $manage_memcached      = $sentry::params::manage_memcached,
  $manage_postgresql     = $sentry::params::manage_postgresql,
  $manage_redis          = $sentry::params::manage_redis,
  $memcached_location    = $sentry::params::memcached_location,
  $organization          = $sentry::params::organization,
  $path                  = $sentry::params::path,
  $redis_host            = $sentry::params::redis_host,
  $redis_port            = $sentry::params::redis_port,
  $single_organization   = $sentry::params::single_organization,
  $system_admin_email    = $sentry::params::system_admin_email,
  $system_secret_key     = $sentry::params::system_secret_key,
  $use_big_ints          = $sentry::params::use_big_ints,
  $use_memcached         = $sentry::params::use_memcached,
  $use_ssl               = $sentry::params::use_ssl,
  $user                  = $sentry::params::user,
  $version               = $sentry::params::version,
  $web_host              = $sentry::params::web_host,
  $web_options           = $sentry::params::web_options,
  $web_port              = $sentry::params::web_port,
) inherits sentry::params {

  if $version != 'latest' {
    if versioncmp('9.0', $version) > 0 {
      fail('Sentry version 9.0 or greater is required')
    }
  }

  # establish resource containment
  contain '::sentry::setup'
  contain '::sentry::config'
  contain '::sentry::install'
  contain '::sentry::service'

  # establish resource precedence and notifications
  Class['::sentry::setup']
  ~> Class['::sentry::config']
  ~> Class['::sentry::install']
  ~> Class['::sentry::service']

  cron { 'sentry cleanup':
    command => "${path}/bin/sentry --config=/etc/sentry cleanup --days=30",
    user    => $user,
    minute  => 15,
    hour    => 1,
    require => Class['::sentry::install'],
  }
}

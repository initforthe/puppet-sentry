# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include sentry::config
class sentry::config (
  # $admin_email           = $sentry::admin_email,
  # $admin_password        = $sentry::admin_password,
  # $admin_user            = $sentry::admin_user,
  # $bootstrap             = $sentry::bootstrap,
  # $custom_config         = $sentry::custom_config,
  # $custom_settings       = $sentry::custom_settings,
  $database_host         = $sentry::database_host,
  $database_name         = $sentry::database_name,
  $database_password     = $sentry::database_password,
  $database_port         = $sentry::database_port,
  $database_user         = $sentry::database_user,
  $debug                 = $sentry::debug,
  # $extensions            = $sentry::extensions,
  $filestore_access_key  = $sentry::filestore_access_key,
  $filestore_backend     = $sentry::filestore_backend,
  $filestore_bucket      = $sentry::filestore_bucket,
  $filestore_location    = $sentry::filestore_location,
  $filestore_secret_key  = $sentry::filestore_secret_key,
  $group                 = $sentry::group,
  $mail_backend          = $sentry::mail_backend,
  $mail_from             = $sentry::mail_from,
  $mail_host             = $sentry::mail_host,
  $mail_mailgun_api_key  = $sentry::mail_mailgun_api_key,
  $mail_password         = $sentry::mail_password,
  $mail_port             = $sentry::mail_port,
  $mail_reply_hostname   = $sentry::mail_reply_hostname,
  $mail_use_tls          = $sentry::mail_use_tls,
  $mail_username         = $sentry::mail_username,
  # $manage_memcached      = $sentry::manage_memcached,
  # $manage_postgresql     = $sentry::manage_postgresql,
  # $manage_redis          = $sentry::manage_redis,
  $memcached_location    = $sentry::memcached_location,
  # $organization          = $sentry::organization,
  $path                  = $sentry::path,
  $redis_host            = $sentry::redis_host,
  $redis_port            = $sentry::redis_port,
  $single_organization   = $sentry::single_organization,
  $system_admin_email    = $sentry::system_admin_email,
  $system_secret_key     = $sentry::system_secret_key,
  $use_big_ints          = $sentry::use_big_ints,
  $use_memcached         = $sentry::use_memcached,
  $use_ssl               = $sentry::use_ssl,
  $user                  = $sentry::user,
  # $version               = $sentry::version,
  $web_host              = $sentry::web_host,
  $web_options           = $sentry::web_options,
  $web_port              = $sentry::web_port,
) {
  assert_private()

  validate_re($filestore_backend, '^(filesystem|gcs|s3)$', "The \$filestore_backend parameter must be set to one of: filesystem, gcs, s3")

  if ($filestore_backend == 'gcs' or $filestore_backend == 's3') and $filestore_bucket == undef {
    fail('$filestore_bucket must be set')
  }

  if $filestore_backend == 's3' {
    if $filestore_access_key == undef {
      fail('$filestore_access_key must be set')
    }

    if $filestore_secret_key == undef {
      fail('$filestore_secret_key must be set')
    }
  }

  file { '/etc/sentry':
    ensure => directory,
    owner  => $user,
    group  => $group,
    mode   => '0755',
  }

  file { '/etc/sentry/sentry.conf.py':
    ensure  => present,
    owner   => $user,
    group   => $group,
    mode    => '0644',
    content => template('sentry/sentry.conf.py.erb'),
    require => File['/etc/sentry'],
  }

  file { '/etc/sentry/config.yml':
    ensure  => present,
    owner   => $user,
    group   => $group,
    mode    => '0644',
    content => template('sentry/config.yml.erb'),
    require => File['/etc/sentry'],
  }
}

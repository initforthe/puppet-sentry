# Sentry installation class
#
# @summary Installation of sentry
#
# @api private
#
class sentry::install (
  $group         = $sentry::group,
  $path          = $sentry::path,
  $use_memcached = $sentry::use_memcached,
  $user          = $sentry::user,
  $version       = $sentry::version,
) {
  assert_private()

  Python::Pip {
    ensure     => present,
    virtualenv => $path,
  }

  python::pip { 'sentry':
    ensure     => $version,
    group      => $group,
    owner      => $user,
    virtualenv => $path,
  }

  if $use_memcached {
    python::pip { 'python-memcached': }
  }
}
